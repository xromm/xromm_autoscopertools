%% Bead Masking

%% prepare for running
close all
clear
clc

%% input file information

lab_drive = 'I:\';

data_folder = [lab_drive 'Brown cadaver data\Cadaver 2018\XROMM\TEST\Images_for_processing\']; 
CSVFile = dir([data_folder '*.csv']);
imageFiles = dir([data_folder '*.tif']);

edited_folder = [lab_drive 'Brown cadaver data\Cadaver 2018\XROMM\TEST\Edited_Images\'];

num_imageFiles = length(imageFiles);
imageFileNames = cell(num_imageFiles, 1);

%% define bead mask variables

diameter_of_bead = 10;

radius_of_mask = round(diameter_of_bead/2);
color_sample_radius = (diameter_of_bead/2) + 2;

%% process each image file

for i=1:num_imageFiles

    %% get image file info
    % file = im; %% TBD 
    [filepath,name,ext] = fileparts(imageFiles(i).name);
  
    current_image_raw = imread([data_folder imageFiles(i).name]);
    
    camera_string = name(17);
    frame_number_string = name(26:29);
    frame_number = str2double(frame_number_string);
    image_name = name;
   
    current_image_double = double(current_image_raw);
    
    %% get .csv file header information
    % needs editing based on file used and how markers are defined

    % read and separate the .csv file data
    [data, colheaders_char, raw_data]=xlsread([data_folder CSVFile.name]);

    %get just first row of text data, which are the column headers
    colheaders = convertCharsToStrings(colheaders_char);

     if camera_string == '1'

         marker1X=(data(:,strcmp(colheaders,'T1_cam1_X')==1));
         marker1Y=(data(:,strcmp(colheaders,'T1_cam1_Y')==1));
         marker2X=(data(:,strcmp(colheaders,'T2_cam1_X')==1));
         marker2Y=(data(:,strcmp(colheaders,'T2_cam1_Y')==1));
         marker3X=(data(:,strcmp(colheaders,'T3_cam1_X')==1));
         marker3Y=(data(:,strcmp(colheaders,'T3_cam1_Y')==1));
         marker4X=(data(:,strcmp(colheaders,'T4_cam1_X')==1));
         marker4Y=(data(:,strcmp(colheaders,'T4_cam1_Y')==1));
         marker5X=(data(:,strcmp(colheaders,'T5_cam1_X')==1));
         marker5Y=(data(:,strcmp(colheaders,'T5_cam1_Y')==1));
         marker6X=(data(:,strcmp(colheaders,'T6_cam1_X')==1));
         marker6Y=(data(:,strcmp(colheaders,'T6_cam1_Y')==1));
         marker7X=(data(:,strcmp(colheaders,'T7_cam1_X')==1));
         marker7Y=(data(:,strcmp(colheaders,'T7_cam1_Y')==1));
         marker8X=(data(:,strcmp(colheaders,'T8_cam1_X')==1));
         marker8Y=(data(:,strcmp(colheaders,'T8_cam1_Y')==1));
         marker9X=(data(:,strcmp(colheaders,'F1_cam1_X')==1));
         marker9Y=(data(:,strcmp(colheaders,'F1_cam1_Y')==1));
         marker10X=(data(:,strcmp(colheaders,'F2_cam1_X')==1));
         marker10Y=(data(:,strcmp(colheaders,'F2_cam1_Y')==1));
         marker11X=(data(:,strcmp(colheaders,'F3_cam1_X')==1));
         marker11Y=(data(:,strcmp(colheaders,'F3_cam1_Y')==1));
         marker12X=(data(:,strcmp(colheaders,'F4_cam1_X')==1));
         marker12Y=(data(:,strcmp(colheaders,'F4_cam1_Y')==1));
         marker13X=(data(:,strcmp(colheaders,'F5_cam1_X')==1));
         marker13Y=(data(:,strcmp(colheaders,'F5_cam1_Y')==1));
         marker14X=(data(:,strcmp(colheaders,'F6_cam1_X')==1));
         marker14Y=(data(:,strcmp(colheaders,'F6_cam1_Y')==1));

     else
         marker1X=(data(:,strcmp(colheaders,'T1_cam2_X')==1));
         marker1Y=(data(:,strcmp(colheaders,'T1_cam2_Y')==1));
         marker2X=(data(:,strcmp(colheaders,'T2_cam2_X')==1)); 
         marker2Y=(data(:,strcmp(colheaders,'T2_cam2_Y')==1));
         marker3X=(data(:,strcmp(colheaders,'T3_cam2_X')==1));
         marker3Y=(data(:,strcmp(colheaders,'T3_cam2_Y')==1));
         marker4X=(data(:,strcmp(colheaders,'T4_cam2_X')==1));
         marker4Y=(data(:,strcmp(colheaders,'T4_cam2_Y')==1));
         marker5X=(data(:,strcmp(colheaders,'T5_cam2_X')==1));
         marker5Y=(data(:,strcmp(colheaders,'T5_cam2_Y')==1));
         marker6X=(data(:,strcmp(colheaders,'T6_cam2_X')==1));
         marker6Y=(data(:,strcmp(colheaders,'T6_cam2_Y')==1));
         marker7X=(data(:,strcmp(colheaders,'T7_cam2_X')==1));
         marker7Y=(data(:,strcmp(colheaders,'T7_cam2_Y')==1));
         marker8X=(data(:,strcmp(colheaders,'T8_cam2_X')==1));
         marker8Y=(data(:,strcmp(colheaders,'T8_cam2_Y')==1));
         marker9X=(data(:,strcmp(colheaders,'F1_cam2_X')==1));
         marker9Y=(data(:,strcmp(colheaders,'F1_cam2_Y')==1));
         marker10X=(data(:,strcmp(colheaders,'F2_cam2_X')==1));
         marker10Y=(data(:,strcmp(colheaders,'F2_cam2_Y')==1));
         marker11X=(data(:,strcmp(colheaders,'F3_cam2_X')==1));
         marker11Y=(data(:,strcmp(colheaders,'F3_cam2_Y')==1));
         marker12X=(data(:,strcmp(colheaders,'F4_cam2_X')==1));
         marker12Y=(data(:,strcmp(colheaders,'F4_cam2_Y')==1));
         marker13X=(data(:,strcmp(colheaders,'F5_cam2_X')==1));
         marker13Y=(data(:,strcmp(colheaders,'F5_cam2_Y')==1));
         marker14X=(data(:,strcmp(colheaders,'F6_cam2_X')==1));
         marker14Y=(data(:,strcmp(colheaders,'F6_cam2_Y')==1));

     end  

     %% create matrix for bead location at current frame
     markerMatrix = [marker1X(frame_number) marker1Y(frame_number);
                    marker2X(frame_number) marker2Y(frame_number);
                    marker3X(frame_number) marker3Y(frame_number);
                    marker4X(frame_number) marker4Y(frame_number);
                    marker5X(frame_number) marker5Y(frame_number);
                    marker6X(frame_number) marker6Y(frame_number);
                    marker7X(frame_number) marker7Y(frame_number);
                    marker8X(frame_number) marker8Y(frame_number);
                    marker9X(frame_number) marker9Y(frame_number);
                    marker10X(frame_number) marker10Y(frame_number);
                    marker11X(frame_number) marker11Y(frame_number);
                    marker12X(frame_number) marker12Y(frame_number);
                    marker13X(frame_number) marker13Y(frame_number);
                    marker14X(frame_number) marker14Y(frame_number);];

    %% set color sampler locations and create bead masks

    [markerMat_row, markerMat_coloumn] = size(markerMatrix);

    for  i = 1:markerMat_row

        centerX = markerMatrix(i,1);
        centerY_XROMM = markerMatrix(i,2);
        % adjust XROMM axis for MATLAB axis 
        centerY = image_limit_Y(1,2) - centerY_XROMM; 

        [current_image_double] = CreateBeadMask(current_image_double, centerX, centerY, radius_of_mask, color_sample_radius);

    end

    %% save edited image 
    
    current_image_edited = uint8(current_image_double);
    imwrite(current_image_edited, fullfile(edited_folder, sprintf('%s-ED.tif',image_name)),'tif')     

end
 