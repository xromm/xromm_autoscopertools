function [sample_matrix] = CreateSampleMaskMatrix(image_matrix, radius, row_value, col_value)

sample_matrix = image_matrix((row_value - radius):(row_value + radius),(col_value - radius):(col_value + radius));