function [im] = CreateBeadMask(im, centerX, centerY, radius_of_mask, color_sample_radius)

% round center values to nearest integer (whole pixel)
centerX_whole = round(centerX);
centerY_whole = round(centerY);

% get pixel color info of 4 color sample areas
color_sample_1_row = centerY_whole;
color_sample_1_col = centerX_whole - (radius_of_mask + color_sample_radius);
color_sample_2_row = centerY_whole - (radius_of_mask + color_sample_radius);
color_sample_2_col = centerX_whole;
color_sample_3_row = centerY_whole;
color_sample_3_col = centerX_whole + (radius_of_mask + color_sample_radius);
color_sample_4_row = centerY_whole + (radius_of_mask + color_sample_radius);
color_sample_4_col = centerX_whole;

sample_matrix_1 = CreateSampleMaskMatrix(im, radius_of_mask, color_sample_1_row, color_sample_1_col);
sample_matrix_2 = CreateSampleMaskMatrix(im, radius_of_mask, color_sample_2_row, color_sample_2_col);
sample_matrix_3 = CreateSampleMaskMatrix(im, radius_of_mask, color_sample_3_row, color_sample_3_col);
sample_matrix_4 = CreateSampleMaskMatrix(im, radius_of_mask, color_sample_4_row, color_sample_4_col);

% take average of 4 sample areas to create mask
mask_matrix = round((sample_matrix_1 + sample_matrix_2 + sample_matrix_3 + sample_matrix_4)./4);

% create the bead mask and place in correct spot
im((centerY_whole - radius_of_mask):(centerY_whole + radius_of_mask), (centerX_whole - radius_of_mask):(centerX_whole + radius_of_mask)) = mask_matrix;

end